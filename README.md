# Musicwish Website for my former middle school

In my former middle school I was part of the Sound & PA Team. Once a year we hosted a disco for the years 5-8. We were in charge for the music and wanted to get in touch with the audience for receiving their music wishes. First we started to track them with a simple paper. Because I just started to get into web development I created this website for sending us the wishe online. It also has an admin view wich generates a list of all wishes and has the possibility to mark a wish as edited.

This was my first project with any kind of database.

I guess the first version of it was created in 2016 or 2017 I don't really know anymore.

## Env vars required to work properly

It is required to set the env vars on the server or create a .env file parser for this php project. Otherwise you need to replace all the getvar() calls with the values.

DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME, EMAIL_ADDRESS, EMAIL_PASSWORD, EMAIL_RECEIVER
