<?php

//Verbindung mit der datenbank aufbauen
$connect = mysqli_connect(getenv("DB_HOST"), getenv("DB_USERNAME"),getenv("DB_PASSWORD"), getenv("DB_NAME"));

//falls es einen fehler gibt, dann zeige ihn mit dem fehlercode an
if(mysqli_connect_errno($connect)){
  echo 'Failed to connect'.mysqli_connect_errno($connect);
}
 ?>
