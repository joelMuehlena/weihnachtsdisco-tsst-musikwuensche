<?php

class DB{
    private $socket_type = "mysql";
    private $host = getenv("DB_HOST");
    private $dbName = getEnv("DB_NAME");
    private $username = getenv("DB_USERNAME");
    private $password = getenv("DB_PASSWORD");

    private $instance = NULL;

    public function __construct(){
        if($this->instance == NULL){
           try{
                $db = new PDO(
                    '' . $this->socket_type . ':host=' . $this->host . ';dbname=' . $this->dbName . '',
                    $this->username, $this->password
                );
                $this->instance = $db;
           }catch(PDOException $err){
               die($err->getMessage());
           }
        }
    }

    public function query($sql){
        $query = $this->instance->prepare($sql);
        $query->execute();
        return $query;
    }
}

?>