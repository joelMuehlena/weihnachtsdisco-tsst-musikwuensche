<?php

class User{
    private $name;
    private $role;
    private $email;

    public function __construct($name, $email, $role = 1){
        $this->name = $name;
        $this->role = $role;
        $this->email = $email;
    }

    public function getName(){
        return $this->name;
    }
	
	 public function getRole(){
        return $this->role;
    }
}

?>