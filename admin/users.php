<?php
require './requires.php';
session_start();
if(!isset($_SESSION["user"])){
    header("Location: ./login.php");
    die();
}
$activeUser = $_SESSION["user"];
$db = new DB;

$sql = "SELECT
  discouser.id,
  discouser.name AS userName,
  discouser.email,
  discoroles.name AS roleName
FROM
  discouser
RIGHT JOIN discoroles ON discouser.roleId = discoroles.id";

$userInfos = $db->query($sql);
$countUsers = $userInfos->rowCount();
$rows = $userInfos->fetchAll();
?>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Users</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="users.css" />
</head>
<body>
    <a href="./">Zurück</a>
    <table>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Email</th>
            <th>Role</th>
        </tr>
        <?php
        foreach ($rows as $row) {
            echo "<tr>";
            echo "<td>". utf8_encode($row["id"])  ."</td>";
            echo "<td>". utf8_encode($row["userName"])  ."</td>";
            echo "<td>". utf8_encode($row["email"])  ."</td>";
            echo "<td>". utf8_encode($row["roleName"])  ."</td>";
            echo "</tr>";
        }
        ?>
    </table>
    <h3 class="totalUsers">Total users: <?php echo $countUsers; ?></h3>
</body>
</html>