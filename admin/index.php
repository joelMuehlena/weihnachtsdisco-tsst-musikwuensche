<?php
require './requires.php';
//include 'Logger.php';
session_start();

if(!isset($_SESSION["user"])){
    header("Location: ./login.php");
    die();
}

$db = new DB;

$activeUser = $_SESSION["user"];

$roleName = $db->query("SELECT name FROM discoroles WHERE id=" . $activeUser->getRole() ."")->fetchAll()[0]["name"];
?>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <style>
    body{
        margin-top: 10px;
    }
    </style>
</head>
<body>
    <div class="container">
        <h2>Hallo, <?php  echo explode(" ", $activeUser->getName())[0]; ?></h2>
		<h4>Du bist ein: <?php  echo $roleName ?></h4>
        <div class="btn-group" role="group" aria-label="Navigation">
            <a href='./tabelle.php'class="btn btn-primary">Tabelle</a>
            <a href='./users.php'class="btn btn-primary">Users</a>
            <a href='http://disco.muehlena.de 'class="btn btn-secondary">Webseite</a>
            <a href='./logout.php' class="btn btn-warning">Logout</a>
        </div>
    </div>
</body>
</html>