<?php
require './requires.php';
session_start();

//include 'Logger.php';

if(!isset($_SESSION["user"])){
    header("Location: ./login.php");
    die();
}

$db = new DB(); 

$activeUser = $_SESSION["user"];


$rolePermission = $db->query("SELECT permissions FROM discoroles WHERE id=" . $activeUser->getRole() ."")->fetchAll()[0]["permissions"];
$decodedPermission = "";
if(trim($rolePermission) !== ""){
	$decodedPermission = json_decode($rolePermission);
}

if(trim($rolePermission) !== "" && $decodedPermission->{'editWish'}){
	$contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';

	if ($contentType === "application/json") {
	  //Receive the RAW post data.
	  $content = trim(file_get_contents("php://input"));

	  $decoded = json_decode($content, true);
	  $elementID = $decoded["payload"];
	  $elementState = $decoded["edited"] == 0 ? 0 : 1;
		
	  $db->query("UPDATE disco SET edited=" . $elementState ." WHERE id=" . $elementID ."");
	 
	}
	
	header('Content-type: application/json');
	http_response_code(200);
	echo json_encode(["success"=> true]);
}else{
	header('Content-type: application/json');
	http_response_code(401);
	echo json_encode(["error"=> "Nicht genug rechte"]);
}


//$roleName = $db->query("SELECT name FROM discoroles WHERE id=" . $activeUser->getRole() ."")->fetchAll()[0]["name"];




?>