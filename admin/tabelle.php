<?php
require './requires.php';
session_start();
if(!isset($_SESSION["user"])){
    header("Location: ./login.php");
    die();
}
$activeUser = $_SESSION["user"];
$db = new DB;
$query = $db->query("SELECT * FROM disco ORDER BY titel ASC");
$countWishes = $query->rowCount();
$wishData = $query->fetchAll();
?>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Wish table</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="tabelle.css" />
</head>
<body>
    <a href="./">Zurück</a>
	<p class="errorText"></p>
    <table>
        <tr>
            <th>ID</th>
            <th>Titel</th>
            <th>Interpret</th>
        </tr>
        <?php
        foreach ($wishData as $row) {
			if($row["edited"] == 1){
				echo "<tr class='tableContent checked' id='" . $row["id"] . "'>";
			}else{
				echo "<tr class='tableContent' id='" . $row["id"] . "'>";
			}
            echo "<td>";
            echo $row["id"];
            echo "</td>";
            echo "<td>";
            echo $row["titel"];
            echo "</td>";
            echo "<td>";
            echo $row["interpret"];
            echo "</td>";
            echo "</tr>";
        }
        ?>
    </table>
    <h3 class="totalWishes">Total wishes: <?php echo $countWishes; ?></h3>
	<script src="./tabelle.js"></script>
</body>
</html>