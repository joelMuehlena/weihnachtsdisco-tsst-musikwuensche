<?php
session_start();
header("Content-Type: text/html; charset=utf-8");
require './requires.php';

$db = new DB();
$loginSystem = new LoginSystem();

if(isset($_POST["email"]) && isset($_POST["password"])){
    $email = $loginSystem->clean($_POST["email"]);
    $password  = $loginSystem->clean($_POST["password"]);

    try{
        $user = $loginSystem->login($email, $password);
        if($user != NULL && count($user) > 0){
            $_SESSION["user"] = new User(utf8_encode($user["name"]), utf8_encode($user["email"]), $user["roleId"]);
            sleep(1);
            header("Location: ./");
        }
    }catch(Exception $er){
        print_r($er);
    }
}

?>

<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Login - Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <form action="#" method="POST">
        <div class="form-group">
            <label for="email">Email address</label>
            <input type="email" class="form-control" name="email">
        </div>
        <div class="form-group">
            <label for="password">Password</label>
            <input type="password" class="form-control" name="password">
        </div>
        <input type="submit" value="Submit" class="btn btn-primary">
    </form>
    </div>
</body>
</html>